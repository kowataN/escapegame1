using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameMain;

public static class ViewLoader
{
    public static GameObject LoadView(ViewType type)
    {

        switch (type)
        {
            case ViewType.Title:
                return Resources.Load("Prefabs/TitleView") as GameObject;

            case ViewType.InGame:
                return Resources.Load("Prefabs/InGameView") as GameObject;

            default:
                return null;
        }
    }
}
