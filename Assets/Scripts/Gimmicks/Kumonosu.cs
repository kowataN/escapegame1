using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Kumonosu : TapGimmick
{
    public UnityEvent _tapCallBack = default;

    private void Start()
    {
        if (SaveManager.Inst.IsClearGimmick(_gimmick))
        {
            Clear();
        }
    }

    public void OnClickThis()
    {
        if (_isClear)
        {
            return;
        }

        if (MessageView.Inst.IsDisplay)
        {
            return;
        }

        if (ItemBox.Inst.TryUseItem(_usedItemType))
        {
            Clear();
            ItemBox.Inst.UseItem(_usedItemType);
            SaveManager.Inst.SetUseItem(_usedItemType);
            SaveManager.Inst.SetClearGimmick(_gimmick);
            _tapCallBack?.Invoke();
            return;
        }

        if (_messages.Messages.Count > 0)
        {
            MessageView.Inst.PlayMessage(_messages);
        }
    }
}
