using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class JewelryButtons : TapGimmick
{
    private enum ButtonColor { Gray, Red, Blue, Yellow, Max };
    [SerializeField] private List<GimmickType> _linkGimmicks = new List<GimmickType>();

    //[SerializeField] private GimmickType _gimmick;
    //[SerializeField] private MessageData _messages = default;
    [SerializeField] private List<Image> _images;
    [SerializeField] private List<Button> _buttons;
    [SerializeField] private List<Color> _imageColor;
    [SerializeField] private List<ButtonColor> _answerColor;

    private List<ButtonColor> _crtColor = new List<ButtonColor>();
    public UnityEvent<bool> ClearCallback;

    //private bool _isClear = false;

    private void Start()
    {
        _isClear = false;
        if (SaveManager.Inst.IsClearGimmick(GimmickType.JewelryBoxButton))
        {
            Clear();
        }

        for (int i = 0; i < _answerColor.Count; ++i)
        {
            ButtonColor col = _isClear == true ? _answerColor[i] : 0;
            _crtColor.Add(col);

            _images[i].color = _imageColor[(int)col];
        }
    }

    public void OnClickThis(int index)
    {
        if (_isClear)
        {
            return;
        }

        if (isClearLinkGimmicks() == false)
        {
            if (_messages.Messages.Count > 0)
            {
                MessageView.Inst.PlayMessage(_messages);
            }
            return;
        }

        ChangeColor(index);
        UpdateColor(index);

        if (IsAllClear())
        {
            //SaveManager.Inst.SetClearGimmick(_gimmick);
            Clear();
        }
    }

    private void ChangeColor(int index)
    {
        _crtColor[index]++;
        if (_crtColor[index] >= ButtonColor.Max)
        {
            _crtColor[index] = ButtonColor.Gray;
        }
    }

    private void UpdateColor(int index)
    {
        int imageIndex = (int)_crtColor[index];
        _images[index].color = _imageColor[imageIndex];
    }

    private bool IsAllClear()
    {
        return _crtColor.SequenceEqual(_answerColor);
    }

    protected void Clear()
    {
        Debug.Log("クリア");
        _isClear = true;
        ClearCallback?.Invoke(_isClear);
    }

    private bool isClearLinkGimmicks()
    {
        foreach (var gimmick in _linkGimmicks)
        {
            if (SaveManager.Inst.IsClearGimmick(gimmick) == false)
            {
                return false;
            }
        }

        return true;
    }
}
