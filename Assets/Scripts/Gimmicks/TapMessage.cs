using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapMessage : MonoBehaviour
{
    [SerializeField] private MessageData? _message = default;

    public void OnClick()
    {
        if (_message?.Messages?.Count > 0)
        {
            MessageView.Inst.PlayMessage(_message);
        }
    }
}
