﻿using UnityEngine;
using UnityEngine.UI;

public class DialLocker : MonoBehaviour
{

    // 表示している画像
    public Image Button0;
    public Image Button1;
    public Image Button2;

    // 画像リソース
    public Sprite MarkSource0;
    public Sprite MarkSource1;
    public Sprite MarkSource2;
    public Sprite MarkSource3;

    // 現在のマーク
    enum MarkType
    {
        kMaru, kSankaku, kHoshi, kDaia
    }
    MarkType _CrtButton0 = MarkType.kMaru;
    MarkType _CrtButton1 = MarkType.kMaru;
    MarkType _CrtButton2 = MarkType.kMaru;



    // ボタンをクリックした時の処理
    public void OnClockButton(int position)
    {
        ChangeMark(position);
        ShowMarkImage(position);
    }

    // ・マーク変数を変更する
    void ChangeMark(int position)
    {
        switch (position)
        {
            case 0:
                _CrtButton0++;
                break;
            case 1:
                _CrtButton1++;
                break;
            case 2:
                _CrtButton2++;
                break;
        }

    }

    // ・マーク変数に応じた画像を表示する
    void ShowMarkImage(int position)
    {
        switch (position)
        {
            case 0:
                Button0.sprite = GetImageSource(_CrtButton0);
                break;
            case 1:
                Button1.sprite = GetImageSource(_CrtButton1);
                break;
            case 2:
                Button2.sprite = GetImageSource(_CrtButton2);
                break;
        }
    }

    Sprite GetImageSource(MarkType type)
    {
        return type switch {
            MarkType.kMaru => MarkSource0,
            MarkType.kSankaku => MarkSource1,
            MarkType.kHoshi => MarkSource2,
            MarkType.kDaia => MarkSource3,
            _ => null,
        };
    }
}
