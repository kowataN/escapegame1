using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapGimmick : MonoBehaviour
{
    [SerializeField] protected ItemType _usedItemType = default;
    [SerializeField] protected GimmickType _gimmick = default;
    [SerializeField] protected MessageData _messages = default;
    [SerializeField] protected bool _isClear = false;

    private void Start()
    {
        if (SaveManager.Inst.IsClearGimmick(_gimmick))
        {
            Clear();
        }
    }

    public void OnClickThis()
    {
        if (_isClear)
        {
            return;
        }

        if (ItemBox.Inst.TryUseItem(_usedItemType))
        {
            Clear();
            ItemBox.Inst.UseItem(_usedItemType);
            SaveManager.Inst.SetUseItem(_usedItemType);
            SaveManager.Inst.SetClearGimmick(_gimmick);
            return;
        }
        
        if (_messages.Messages.Count > 0)
        {
            MessageView.Inst.PlayMessage(_messages);
        }
    }

    protected void Clear()
    {
        gameObject.SetActive(false);
        _isClear = true;
    }
}
