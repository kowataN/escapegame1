using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class JewelryKey : TapGimmick
{
    [SerializeField] private GimmickType _unlockGimmick = default;
    [SerializeField] private GameObject _closeBox = default;
    [SerializeField] private GameObject _openBox = default;
    private bool _isUnlock = false;

    private void Start()
    {
        _isUnlock = SaveManager.Inst.IsClearGimmick(_unlockGimmick);
        if (SaveManager.Inst.IsClearGimmick(_gimmick))
        {
            Clear();
        }
    }

    public void OnClickThis()
    {
        if (_isClear || _isUnlock)
        {
            return;
        }

        if (ItemBox.Inst.TryUseItem(_usedItemType))
        {
            if (_messages.Messages.Count > 0)
            {
                MessageView.Inst.PlayMessage(_messages);
            }

            //Clear();
            ItemBox.Inst.UseItem(_usedItemType);
            SaveManager.Inst.SetUseItem(_usedItemType);
            SaveManager.Inst.SetClearGimmick(_unlockGimmick);
            return;
        }
    }

    public void Clear()
    {
        _isClear = true;
        _closeBox.SetActive(!_isClear);
        _openBox.SetActive(_isClear);
    }
}
