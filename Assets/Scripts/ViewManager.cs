using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ViewManager
{
    public static void SetVisibleView(GameObject obj, bool value)
    {
        obj.SetActive(value);
    }
}
