using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMain : MonoBehaviour
{
    [SerializeField] GameObject _viewParent = default;
    [SerializeField] GameObject _crtView = default;

    private void Start()
    {
        ViewManager.SetVisibleView(_crtView, true);
    }
}
