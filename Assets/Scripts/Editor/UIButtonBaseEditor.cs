using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.UI;
#endif

#if UNITY_EDITOR
[CustomEditor(typeof(UIButtonBase))]
public class UIButtonBaseEditor : ButtonEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var component = (UIButtonBase)target;

        component._clickSEFileName = EditorGUILayout.TextField("Click SE FileName", component._clickSEFileName);

        serializedObject.ApplyModifiedProperties();
    }
}
#endif