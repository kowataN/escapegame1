using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public sealed class TitleView : ViewBase
{
    [SerializeField] private Button _startButton = default;
    [SerializeField] private Button _continueButton = default;

    public Button StartButton => _startButton;
    public Button ContinueButton => _continueButton;

    private void Start()
    {
        _continueButton.interactable = SaveManager.Inst.HasSaveData();
    }

    public void OnStart()
    {
        SaveManager.Inst.CreateNewData();
        SceneManager.LoadScene("Main");
    }

    public void OnContinue()
    {
        SaveManager.Inst.Load();
        SceneManager.LoadScene("Main");
    }
}
