using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonView : MonoBehaviour
{
    [SerializeField] private UIButtonBase _menuButton;

    private void Start()
    {
        _menuButton.SetClickEvent(OnClickMenuButton);
    }

    private void OnClickMenuButton()
    {
        Debug.Log("OnClick MenuButton");
    }
}
