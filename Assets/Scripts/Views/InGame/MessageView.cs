using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageView : SingletonMonoBhv<MessageView>
{
    [SerializeField] private GameObject _frame = default;
    [SerializeField] private Text _crtText = default;
    [SerializeField] private Text _tapIcon = default;
    private string _dispMessage = default;
    private Action _callback = default;

    // 確認の為一旦インスペクターに表示
    [SerializeField] private MessageData _msgData = default;
    [SerializeField] int _crtLineNumber = 0;

    public enum TypingMode
    {
        Idle, Typing
    }
    [SerializeField] private TypingMode _typingMode = TypingMode.Idle;
    public bool IsTyping => _typingMode == TypingMode.Typing;

    private Coroutine _crtCoroutine = default;

    private void Awake()
    {
        SetVisible(false);
        _tapIcon.gameObject.SetActive(false);
    }

    public void ResetMessage() => _crtText.text = "";

    public void SetVisible(bool value) => _frame.SetActive(value);

    public bool IsDisplay => _frame.activeSelf;

    public void PlayMessage(MessageData data, Action callback = null)
    {
        _msgData = data;
        _crtLineNumber = 0;
        _callback = callback;
        SetVisible(true);
        StartMessgeTyping();
    }

    private IEnumerator TypeMessage(string msg)
    {
        _tapIcon.gameObject.SetActive(false);
        _crtText.text = "";
        _dispMessage = msg;
        _typingMode = TypingMode.Typing;
        foreach (char c in msg.ToCharArray())
        {
            _crtText.text += c;
            yield return new WaitForSeconds(1f / 15);
        }
        _typingMode = TypingMode.Idle;
        _tapIcon.gameObject.SetActive(true);
    }

    public void OnClickMessageView()
    {
        switch (_typingMode)
        {
            case MessageView.TypingMode.Typing:
                StopCoroutine(_crtCoroutine);
                _typingMode = TypingMode.Idle;
                _crtText.text = _dispMessage;
                _tapIcon.gameObject.SetActive(true);
                break;

            case TypingMode.Idle:
                if (_crtLineNumber < _msgData.Messages.Count - 1)
                {
                    _crtLineNumber++;
                    StartMessgeTyping();
                }
                else
                {
                    SetVisible(false);
                    _msgData = null;
                    _callback?.Invoke();
                    _callback = null;
                }
                break;
        }
    }

    private void StartMessgeTyping()
    {
        _crtCoroutine = StartCoroutine(TypeMessage(_msgData.Messages[_crtLineNumber]));
    }
}
