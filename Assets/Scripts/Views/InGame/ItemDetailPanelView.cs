using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDetailPanelView : MonoBehaviour
{
    [SerializeField] Text _name = default;
    [SerializeField] Text _detail = default;
    [SerializeField] Image _image = default;
    [SerializeField] Button _closeButton = default;
    [SerializeField] Button _imageButton = default;

    ItemModel _itemModel = default;
    public Action OnClickClose = default;
    public Action OnClickImage = default;

    public void ShowPanel(ItemModel item)
    {
        _itemModel = item;

        _name.text = _itemModel.Name;
        _detail.text = _itemModel.Comment;
        _image.sprite = _itemModel.Image[0];

        gameObject.SetActive(true);
    }

    public void OnClickCloseButton()
    {
        OnClickClose?.Invoke();
        gameObject.SetActive(false);

        ItemBox.Inst.IsTouchEnabled = true;
    }

    public void OnClickImageButton()
    {
        OnClickImage?.Invoke();
    }
}
