using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TansuView : MonoBehaviour
{
    [SerializeField] private GameObject _openObj = default;

    public void OnClick() => _openObj.SetActive(!_openObj.activeSelf);
    public void SetInvisibleOpenObj() =>_openObj.SetActive(false);
}
