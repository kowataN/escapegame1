using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallWindowView : MonoBehaviour
{
    [SerializeField] private ItemType _hasItemType = default;
    [SerializeField] private MessageData _message1 = default;
    [SerializeField] private MessageData _useMag = default;
    [SerializeField] private MessageData _hasMsg = default;

    public void OnClickWindow()
    {
        if (MessageView.Inst.IsDisplay)
        {
            return;
        }

        if (ItemBox.Inst.TryUseItem(_hasItemType))
        {
            MessageView.Inst.PlayMessage(_useMag, () => {
                ItemBox.Inst.UseItem(_hasItemType);
                SaveManager.Inst.SetUseItem(_hasItemType);
            });
        }
        else if (ItemBox.Inst.HasItem(_hasItemType))
        {
            MessageView.Inst.PlayMessage(_hasMsg);
        }
        else
        {
            MessageView.Inst.PlayMessage(_message1);
        }
    }
}
