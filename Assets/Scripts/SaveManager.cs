using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SaveManager : SingletonMonoBhv<SaveManager>
{
    const string SAVE_DATA = "SAVE_DATA";
    [SerializeField] private SaveData _saveData = default;

    private void Save()
    {
        string json = JsonUtility.ToJson(_saveData);
        PlayerPrefs.SetString(SAVE_DATA, json);
    }

    public void Load()
    {
        if (HasSaveData())
        {
            string json = PlayerPrefs.GetString(SAVE_DATA);
            _saveData = JsonUtility.FromJson<SaveData>(json);
        }
        else
        {
            CreateNewData();
        }
    }

    public void CreateNewData()
    {
        PlayerPrefs.DeleteKey(SAVE_DATA);
        _saveData = new SaveData();
    }

    public bool HasSaveData() => PlayerPrefs.HasKey(SAVE_DATA);

    public void SetItemBox(ItemType type)
    {
        if (_saveData.ItemBox.Exists(x => x == type))
        {
            return;
        }

        _saveData.ItemBox.Add(type);
        Save();
    }

    public void SetUseItem(ItemType type)
    {
        if (_saveData.UseItem.Exists(x => x == type))
        {
            return;
        }

        _saveData.UseItem.Add(type);
        Save();
    }

    public void SetShowMessage(MessageType type)
    {
        if (_saveData.ShowMsg.Exists(x => x == type))
        {
            return;
        }

        _saveData.ShowMsg.Add(type);
        Save();
    }

    public void SetClearGimmick(GimmickType type)
    {
        if (_saveData.ClearGimmick.Exists(x => x == type))
        {
            return;
        }

        _saveData.ClearGimmick.Add(type);
        Save();
    }

    public bool IsShowMessage(MessageType type) =>_saveData.ShowMsg.Exists(x => x == type);
    public bool IsItemBox(ItemType type) => _saveData.ItemBox.Exists(x => x == type);
    public bool IsUseItem(ItemType type) => _saveData.UseItem.Exists(x => x == type);
    public bool IsClearGimmick(GimmickType type) => _saveData.ClearGimmick.Exists(x => x == type);
}
