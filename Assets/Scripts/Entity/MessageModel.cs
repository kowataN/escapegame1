using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Entity/MessageModel")]
public class MessageModel : ScriptableObject {
    [Header("TYPE")]
    public MessageType Type;

    [Header("メッセージ一覧")]
    public MessageData Messages;
}
