using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public List<ItemType> ItemBox = new List<ItemType>();
    public List<ItemType> UseItem = new List<ItemType>();
    public List<MessageType> ShowMsg = new List<MessageType>();
    public List<GimmickType> ClearGimmick = new List<GimmickType>();
}
