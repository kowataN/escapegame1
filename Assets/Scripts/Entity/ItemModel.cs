using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Entity/ItemModel")]
public class ItemModel : ScriptableObject {
    [Header("種類")]
    public ItemType ItemType = ItemType.Bread;

    [Header("名称")]
    public string Name;

    [Header("コメント")]
    public string Comment;

    [Header("画像")]
    public List<Sprite> Image;

    public ItemModel(ItemModel model) {
        this.ItemType = model.ItemType;
        this.Name = model.Name;
        this.Comment = model.Comment;
        this.Image = model.Image;
    }
}
