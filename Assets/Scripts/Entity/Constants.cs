public enum ViewType
{
    Title,
    InGame,
    End,

    Max
}

/// <summary>
/// アイテムの種類
/// </summary>
public enum ItemType
{
    /// <summary>
    /// なし
    /// </summary>
    None = 0,
    /// <summary>
    /// 食べ残しのパン
    /// </summary>
    Bread,
    /// <summary>
    /// 暗号メモ
    /// </summary>
    Memo,
    /// <summary>
    /// 鉛筆
    /// </summary>
    Pencil,
    /// <summary>
    /// 古いチラシ
    /// </summary>
    Flyer,
    /// <summary>
    /// 手鏡1
    /// </summary>
    HandMirror1,
    /// <summary>
    /// 手鏡2
    /// </summary>
    HandMirror2,
    /// <summary>
    /// はたき
    /// </summary>
    Knock,
    /// <summary>
    /// 鍵
    /// </summary>
    Key,
    /// <summary>
    /// 栞
    /// </summary>
    Boolmark,
    /// <summary>
    /// 魔法のロープ
    /// </summary>
    Rope,
    /// <summary>
    /// 布巾
    /// </summary>
    Cloth,
    /// <summary>
    /// 金貨の袋
    /// </summary>
    Bag,

    Max
}

/// <summary>
/// ギミック
/// </summary>
public enum GimmickType
{
    /// <summary>
    /// なし
    /// </summary>
    None = 0,
    /// <summary>
    /// 小窓にパンを置く
    /// </summary>
    UseBread,
    /// <summary>
    /// 蜘蛛の巣を消す
    /// </summary>
    RemoveKumonosu,
    /// <summary>
    /// 宝石箱のボタン
    /// </summary>
    JewelryBoxButton,
    /// <summary>
    /// 宝石箱の鍵
    /// </summary>
    JewelryBoxKey,

    Max
}

/// <summary>
/// メッセージ表示
/// </summary>
public enum MessageType
{
    /// <summary>
    /// オープニング
    /// </summary>
    Opening,
    /// <summary>
    /// 本棚
    /// </summary>
    BookShelf,
    /// <summary>
    /// タンスの引き出しオープン
    /// </summary>
    TansuOpend,

    Max
}