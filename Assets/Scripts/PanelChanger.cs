﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// パネルの表示を更新
/// </summary>
public class PanelChanger : MonoBehaviour
{
    [SerializeField] private GameObject _leftArrow = default;
    [SerializeField] private GameObject _rightArrow = default;
    [SerializeField] private GameObject _backArrow = default;
    
    [SerializeField] private MessageModel _moveBookShelfMsg = default;
    [SerializeField] private MessageModel _moveTansuUp2Msg = default;

    public UnityEvent TransitCallbackTansuUp = new UnityEvent();

    public enum PanelType
    {
        // 小屋1
        Warehouse1 = 0,
        // 小屋2
        Warehouse2,
        // 小窓アップ
        SmallWindowUp,
        // 本棚アップ
        Bookshelf,
        // タンスアップ
        TansuUp,
        // 本アップ
        BookUp,
        // 箱アップ
        BoxUp,

        Max
    }
    private PanelType _crtPanel = PanelType.Warehouse1;

    struct PanelInfo
    {
        public Vector2 Position;
        public bool ShowLeftArrow;
        public bool ShowRightArrow;
        public bool ShowBackArrow;

        public PanelInfo(Vector2 pos, bool showLeft, bool showRight, bool showBack)
        {
            this.Position = pos;
            this.ShowLeftArrow = showLeft;
            this.ShowRightArrow = showRight;
            this.ShowBackArrow = showBack;
        }
    };
    Dictionary<PanelType, PanelInfo> _panelInfos = new Dictionary<PanelType, PanelInfo>();


    private void Awake()
    {
        RegistPanel();

        ShowNextPanel(PanelType.Warehouse1);
    }

    private void RegistPanel()
    {
        _panelInfos.Add(PanelType.Warehouse1, new PanelInfo(new Vector2(0, 0), false, true, false));
        _panelInfos.Add(PanelType.Warehouse2, new PanelInfo(new Vector2(-1000, 0), true, false, false));
        _panelInfos.Add(PanelType.SmallWindowUp, new PanelInfo(new Vector2(0, 1500), false, false, true));
        _panelInfos.Add(PanelType.Bookshelf, new PanelInfo(new Vector2(-1000, 1500), false, false, true));
        _panelInfos.Add(PanelType.TansuUp, new PanelInfo(new Vector2(-2000, 1500), false, false, true));
        _panelInfos.Add(PanelType.BookUp, new PanelInfo(new Vector2(-1000, 3000), false, false, true));
        _panelInfos.Add(PanelType.BoxUp, new PanelInfo(new Vector2(-1000, 4500), false, false, true));
    }

    private void ShowNextPanel(PanelType panel)
    {
        // メッセージウインドウ表示中は画面遷移させない
        if (MessageView.Inst.IsDisplay)
        {
            return;
        }

        _crtPanel = panel;
        this.transform.localPosition = _panelInfos[_crtPanel].Position;
        _leftArrow.SetActive(_panelInfos[_crtPanel].ShowLeftArrow);
        _rightArrow.SetActive(_panelInfos[_crtPanel].ShowRightArrow);
        _backArrow.SetActive(_panelInfos[_crtPanel].ShowBackArrow);

    }

    public void OnLeftArrow()
    {
        var nextType = _crtPanel switch {
            PanelType.Warehouse2 => PanelType.Warehouse1,
            0 => throw new System.NotImplementedException(),
        };

        ShowNextPanel(nextType);
    }

    public void OnRightArrow() => ShowNextPanel(PanelType.Warehouse2);

    public void OnBackArrow()
    {
        switch (_crtPanel)
        {
            case PanelType.SmallWindowUp:
                ShowNextPanel(PanelType.Warehouse1);
                break;
            case PanelType.Bookshelf:
                ShowNextPanel(PanelType.Warehouse2);
                break;
            case PanelType.BoxUp:
                ShowNextPanel(PanelType.Bookshelf);
                break;
            case PanelType.TansuUp:
                TransitCallbackTansuUp?.Invoke();
                ShowNextPanel(PanelType.Warehouse2);
                break;
            case PanelType.BookUp:
                ShowNextPanel(PanelType.Bookshelf);
                break;
        }
    }

    public void OnSmallWindow()
    {
        ShowNextPanel(PanelType.SmallWindowUp);
    }

    /// <summary>
    /// 本棚アップへ遷移
    /// </summary>
    public void OnClickBookshelf()
    {
        ShowNextPanel(PanelType.Bookshelf);

        if (!SaveManager.Inst.IsShowMessage(_moveBookShelfMsg.Type))
        {
            MessageView.Inst.PlayMessage(_moveBookShelfMsg.Messages, () => {
                SaveManager.Inst.SetShowMessage(_moveBookShelfMsg.Type);
            });
        }
    }

    public void OnClickTansu()
    {
        ShowNextPanel(PanelType.TansuUp);
    }

    public void OnClickBox()
    {
        ShowNextPanel(PanelType.BoxUp);
    }
    public void OnClickBook()
    {
        ShowNextPanel(PanelType.BookUp);
    }
}
