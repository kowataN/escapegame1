using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePresenter : MonoBehaviour
{
    [SerializeField] InGameView _view = default;
    [SerializeField] ItemDetailPanelView _itemDetailView = default;
    [SerializeField] private MessageModel _openingMsg = default; // 一旦ここで

    void Awake()
    {
        SaveManager.Inst.Load();

        // アイテム詳細パネル読み込み
        ItemBox.Inst.OnShowItemDetail += _itemDetailView.ShowPanel;

        _itemDetailView.gameObject.SetActive(false);
    }

    private void Start()
    {
        // オープニングメッセージ表示
        if (!SaveManager.Inst.IsShowMessage(_openingMsg.Type))
        {
            MessageView.Inst.PlayMessage(_openingMsg.Messages, () => {
                SaveManager.Inst.SetShowMessage(_openingMsg.Type);
            });
        }
    }
}
