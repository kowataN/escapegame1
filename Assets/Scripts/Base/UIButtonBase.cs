using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonBase : Button
{
    [SerializeField] public string _clickSEFileName;

    protected override void Awake()
    {
        base.Awake();
    }

    public void SetClickEvent(System.Action onClickAction)
    {
        this.onClick.RemoveAllListeners();
        this.onClick.AddListener(() => {
            Debug.Log($"{_clickSEFileName}");
            onClickAction?.Invoke();
        });
    }
}
