using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ItemListEntity : ScriptableObject {
    public List<ItemModel> _ItemList = new List<ItemModel>();
}
