﻿using UnityEngine;

public class ItemObject : MonoBehaviour
{
    // クリックした時にアイテムボックスに格納し、非表示にする
    [SerializeField] protected ItemType itemType = default;
    [SerializeField] private MessageData _messageData = default;
    [SerializeField] protected ItemModel _itemModel;

    private void Awake()
    {
        _itemModel = ItemGenerater.Inst.Spawn(itemType);
    }

    private void Start()
    {
        SetVisibleToSaveData();
    }

    public void SetVisibleToSaveData()
    {
        // 既に所持か使用していたら表示しない
        bool isUseItem = SaveManager.Inst.IsUseItem(_itemModel.ItemType);
        if (isUseItem)
        {
            SetVisible(false);
            return;
        }

        bool isItemBox = SaveManager.Inst.IsItemBox(_itemModel.ItemType);
        if (isItemBox)
        {
            SetItemToItemBox();
        }
    }

    public void OnClickThis()
    {
        Debug.Log("OnClickThis Item");
        if (MessageView.Inst.IsDisplay)
        {
            return;
        }

        // メッセージがある場合は表示する
        if (_messageData.Messages?.Count > 0)
        {
            MessageView.Inst.PlayMessage(_messageData, () => {
                SetItemToItemBox();
            });
        }
        else
        {
            SetItemToItemBox();
        }
    }

    protected void SetItemToItemBox()
    {
        ItemBox.Inst.SetItem(_itemModel);
        SetVisible(false);
        SaveManager.Inst.SetItemBox(_itemModel.ItemType);
    }

    protected void SetVisible(bool value) => gameObject.SetActive(value);
}
