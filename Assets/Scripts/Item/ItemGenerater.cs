using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemGenerater : SingletonMonoBhv<ItemGenerater> {
    [SerializeField] ItemListEntity _ItemListEntity = default;

    public ItemModel Spawn(ItemType type) {
        return _ItemListEntity._ItemList.Where(x => x.ItemType == type)
                                        .FirstOrDefault();
    }

    public GameObject GetZoomItem(ItemType type) {
/*        foreach (Item item in _ItemListEntity._ItemList) {
            if (item._Type == type) {
                return item._ZoomObj;
            }
        }*/
        return null;
    }
}
