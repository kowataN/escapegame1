﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    [SerializeField] private Image _image = default;
    [SerializeField] private GameObject _bgPanel = default;
    [SerializeField] private GameObject _zoomButton = default;

    public Action<ItemModel> OnClickZoom = default;

    private ItemModel _itemModel = default;
    public ItemModel Item => _itemModel;

    public bool IsEmpty
    {
        get => _itemModel == null;
    }

    private void Start()
    {
        SetSelectActive(false);
    }

    private void SetSelectActive(bool value)
    {
        _bgPanel.SetActive(value);
        _zoomButton.SetActive(value);
    }

    public void SetItem(ItemModel item)
    {
        _itemModel = item;
        UpdateImage(item);
    }

    public ItemModel GetItem() => _itemModel;

    public void UpdateImage(ItemModel item) => _image.sprite = item != null ? item.Image[0] : null;

    public bool OnSelected()
    {
        if (_itemModel == null)
        {
            return false;
        }
        SetSelectActive(true);
        return true;
    }

    public void HideBGPanel() => SetSelectActive(false);

    public void OnClickZoomButton() => OnClickZoom?.Invoke(_itemModel);
}
