﻿using System;
using System.Linq;
using UnityEngine;

public class ItemBox : SingletonMonoBhv<ItemBox>
{
    [SerializeField] private Slot[] _slots = default;
    private Slot _selectedSlot = default;
    public Action<ItemModel> OnShowItemDetail = default;
    public bool IsTouchEnabled { get; set; }
    public Slot[] Slots => _slots;

    private void Awake()
    {
        _slots = GetComponentsInChildren<Slot>();
        IsTouchEnabled = true;
    }

    public void SetItem(ItemModel item)
    {
        Slot? slot = _slots.Where(x => x.IsEmpty)
                           .FirstOrDefault();
        slot?.SetItem(item);
    }

    public void OnSelectedSlot(int position)
    {
        if (!IsTouchEnabled)
        {
            return;
        }

        _slots.ToList()
              .ForEach(x => x.HideBGPanel());

        if (_slots[position].OnSelected())
        {
            // 同じアイテムを選択した場合は詳細を開く
            if (_selectedSlot?.GetItem().Name == _slots[position].GetItem().Name)
            {
                OnShowItemDetail?.Invoke(_selectedSlot?.GetItem());
                IsTouchEnabled = false;
            }

            _selectedSlot = _slots[position];
        }
    }

    /// <summary>
    /// 指定アイテムを持っているかを返します。
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool HasItem(ItemType type)
    {
        return _slots.ToList()
                     .Where(x => x.Item?.ItemType == type)
                     .Any();
    }

    /// <summary>
    /// 指定アイテムを使用できるかを返します。
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool TryUseItem(ItemType type)
    {
        if (_selectedSlot == null)
        {
            return false;
        }

        return _selectedSlot.GetItem().ItemType == type;
    }

    /// <summary>
    /// 指定アイテムを使用します。
    /// </summary>
    /// <param name="type"></param>
    public void UseItem(ItemType type)
    {
        if (_selectedSlot.GetItem().ItemType != type)
        {
            return;
        }

        _selectedSlot.SetItem(null);
        _selectedSlot.HideBGPanel();
        _selectedSlot = null;
    }

    public ItemModel GetSeletectedItem()
    {
        return _selectedSlot == null ? null : _selectedSlot.GetItem();
    }
}
