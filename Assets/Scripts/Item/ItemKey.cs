using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class ItemKey : ItemObject
{
    [SerializeField] private GameObject _kirakira = default;
    [SerializeField] private ItemType _usedItemType = default;

    private void Awake()
    {
        _itemModel = ItemGenerater.Inst.Spawn(itemType);
    }

    private void Start()
    {
        // 既に所持か使用していたら表示しない
        bool isUseItem = SaveManager.Inst.IsUseItem(_itemModel.ItemType);
        if (isUseItem)
        {
            SetVisible(false);
            return;
        }

        // 鍵を持っているか
        bool isItemBox = SaveManager.Inst.IsItemBox(_itemModel.ItemType);
        if (isItemBox)
        {
            SetItemToItemBox();
            return;
        }

        // はたきを使用していたらキラキラ表示
        ShowKirakira();
    }

    public void ShowKirakira()
    {
        _kirakira.SetActive(SaveManager.Inst.IsUseItem(_usedItemType));
    }
}
